import torch
import torch.nn.functional as F
import torch.nn as nn
from einops import rearrange, reduce, repeat
from torch import Tensor

class Residual(nn.Module):
    def __init__(self, in_c, out_c, conv_, if_1d = False , use_1x1conv=False):
        super(Residual, self).__init__()
        self.k, self.s, self.p,self.g = None, None, 1, 1
        if conv_.shape == 2:
            self.k,self.s = conv_
        if conv_.shape == 3:
            self.k,self.s, self.p = conv_
        if conv_.shape == 4:
            self.k,self.s, self.p,self.g = conv_

        self.Sequential = None
        
        if if_1d:
            self.Sequential = nn.Sequential(
            nn.Conv1d(in_c,out_c, kernel_size=self.k, stride=self.s,padding=self.p,groups=self.g),
            nn.BatchNorm1d(out_c), 
            nn.ReLU(inplace=True),
            nn.Conv1d(out_c,out_c,kernel_size=self.k, stride=self.s,padding=self.p,groups=self.g),
            nn.BatchNorm1d(out_c))
        else:     
            self.Sequential = nn.Sequential(
                nn.Conv2d(in_c,out_c, kernel_size=self.k, stride=self.s,padding=self.p,groups=self.g),
                nn.BatchNorm2d(out_c,), 
                nn.ReLU(inplace=True),
                nn.Conv2d(out_c,out_c,kernel_size=self.k, stride=self.s,padding=self.p,groups=self.g),
                nn.BatchNorm2d(out_c))
        
        if use_1x1conv:
            if if_1d:
                self.downsample = nn.Conv1d(in_c, out_c, kernel_size=1, groups= self.g)
            else: 
                self.downsample = nn.Conv2d(in_c, out_c, kernel_size=1, groups=self.g)
        else:
            self.downsample = None

    def forward(self, x: Tensor) -> Tensor:
        identity = x  
        out = self.Sequential(x)
        
        if self.downsample is not None:
            identity = self.downsample(x)
        out += identity
        out = F.relu(out)
        return out

def RB(self, in_c, out_c, num_residuals, conv_, first_block=False):
    blk = []
    for i in range(num_residuals):
        if i == 0 and not first_block:
            blk.append(Residual(in_c, out_c, conv_, use_1x1conv=True))
        else:
            blk.append(Residual(out_c, out_c, conv_))
    return blk

'depth-wise cnn wight groups = n'
class CNN_simple(nn.Module):
    def __init__(self,in_c, c):
        super(CNN_simple, self).__init__()    
        self.Sequential = nn.Sequential(
            nn.Conv2d(in_channels=in_c, out_channels=c, kernel_size=(5,5), stride=1,  padding=(2,2)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),nn.MaxPool2d((4,4)),
            nn.Conv2d(in_channels=c,    out_channels=c,kernel_size=(3,3),   stride=1, padding=(1,1)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=c,    out_channels=c, kernel_size=(3,3),  stride=1, padding=(1,1)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=c,    out_channels=c, kernel_size=(3,3),  stride=1, padding=(1,1)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=c,    out_channels=c, kernel_size=(3,3),  stride=1, padding=(1,1)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),
            nn.AdaptiveAvgPool2d((1,1)),
            nn.Flatten())
        self.dropout = nn.Dropout(0.3)
        self.fc1 = nn.Linear(c, 64)
        self.fc2 = nn.Linear(64, 2)
        
    def forward(self, x: Tensor) -> Tensor:
        out = self.Sequential(x)
        out = F.relu(self.fc1(out))
        out = self.dropout(out)
        out = self.fc2(out)
        return out


'using resnet-18'
class conv_lstm_resnet(nn.Module):
    def __init__(self, n, k, hiddens=128,n_classes=2, bid=False):
        super(conv_lstm_resnet, self).__init__()  
        
        self.conv = [(3,3),1, (2,2)]

        self.cnn = nn.Sequential(
            nn.Conv1d(in_channels=1, out_channels=64, kernel_size=(5,5), stride=1, padding=(3,3)),
            nn.BatchNorm1d(64), nn.ReLU(inplace=True), nn.MaxPool1d(4,4),
            
            *self.RB(64, 64, 2, self.conv, first_block=True),
            *self.RB(64, 128, 2, self.conv),
            *self.RB(128, 256, 2, self.conv),
            *self.RB(256, 512, 2, self.conv),
            nn.AdaptiveAvgPool1d(1),
            nn.Flatten())

        self.hiddens = hiddens
        self.lstm = nn.LSTM(k, self.hiddens, num_layers=2, dropout=0.2, batch_first=False, bidirectional=bid)
        self.n = n
        self.dropout = nn.Dropout(0.3)
        
        self.fc1 = nn.Linear(hiddens, n_classes)
        if bid:
            self.fc1 = nn.Linear(hiddens*2, n_classes)

        
    def forward(self, x: Tensor) -> Tensor:
        "1 11 128 128"
        out = self.cnn(x[:,0,::]).unsqueeze(1)
        for i in range(1, self.n):
            out = torch.tensor(torch.stack(self.cnn(x[:,i,::]).unsqueese(1),1))
        out = rearrange(out, 'b (c g)  -> b c g', c = self.n)
        #out = torch.stack(torch.chunk(out, self.n, 1), 1)
        out,(_,_) = self.lstm(out.permute(1, 0, 2))
        out = out[-1,:,:]
        out = self.fc1(out)
        return out


class conv_lstm(nn.Module):
    def __init__(self, n, k, hiddens=128,n_classes=2, bid=False):
        super(conv_lstm, self).__init__()  

        self.seq_cnn = nn.Sequential(
            nn.Conv2d(in_channels=n, out_channels= k*n,   kernel_size=(5,5), stride=1,padding=(2,2),groups=n),
            nn.BatchNorm2d(k*n), nn.ReLU(inplace=True), nn.MaxPool2d((2,2)),
            nn.Conv2d(in_channels=k*n, out_channels= k*n, kernel_size=(3,3), stride=1,padding=(1,1),groups=n),
            nn.BatchNorm2d(k*n), nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=k*n, out_channels= k*n, kernel_size=(3,3), stride=1,padding=(1,1),groups=n),
            nn.BatchNorm2d(k*n), nn.ReLU(inplace=True), 
            nn.Conv2d(in_channels=k*n, out_channels= k*n, kernel_size=(3,3), stride=1,padding=(1,1),groups=n),
            nn.BatchNorm2d(k*n), nn.ReLU(inplace=True), 
            nn.AdaptiveAvgPool2d((1,1)),
            nn.Flatten(1)
            )
        
        self.hiddens = hiddens
        self.lstm = nn.LSTM(k, self.hiddens, num_layers=2, dropout=0.2, batch_first=False, bidirectional=bid)
        self.n = n
        self.dropout = nn.Dropout(0.3)
        
        self.fc1 = nn.Linear(hiddens, n_classes)
        if bid:
            self.fc1 = nn.Linear(hiddens*2, n_classes)

        
    def forward(self, x: Tensor) -> Tensor:
        out = self.seq_cnn(x)
        out = rearrange(out, 'b (c g)  -> b c g', c = self.n)
        #out = torch.stack(torch.chunk(out, self.n, 1), 1)
        out,(_,_) = self.lstm(out.permute(1, 0, 2))
        out = out[-1,:,:]
        out = self.fc1(out)
        return out


'fusion with mfcc_mean and chroma'
class Model4(nn.Module):
    def __init__(self,in_c, c, n_mfcc, n_chroma):
        super(Model4, self).__init__()    
        self.Sequential = nn.Sequential(
            nn.Conv2d(in_channels=in_c, out_channels=c, kernel_size=(5,9), stride=1,  padding=(3,5)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),nn.MaxPool2d((2,4)),
            nn.Conv2d(in_channels=c,    out_channels=c,kernel_size=(3,9),   stride=1, padding=(1,5)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),nn.MaxPool2d((2,4)),
            nn.Conv2d(in_channels=c,    out_channels=c, kernel_size=(3,9),  stride=1, padding=(1,5)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),nn.MaxPool2d((2,4)),
            nn.Conv2d(in_channels=c,    out_channels=c, kernel_size=(3,9),  stride=1, padding=(1,5)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),nn.MaxPool2d((2,4)),
            nn.Conv2d(in_channels=c,    out_channels=c, kernel_size=(3,9),  stride=1, padding=(1,5)),
            nn.BatchNorm2d(c), nn.ReLU(inplace=True),nn.MaxPool2d((2,4)),
            nn.AdaptiveAvgPool2d((1,1)),
            nn.Flatten())
        self.dropout = nn.Dropout(0.3)
        self.fc_norm = nn.LayerNorm(64 + 128 + 12)
        self.fc1 = nn.Linear(64 + 128 + 12, 128)
        
        self.fc2 = nn.Linear(128, 2)
        
    def forward(self, x: Tensor, mfcc:Tensor, chroma:Tensor) -> Tensor:
        x = self.Sequential(x)
        #print(chroma.shape) 
        out = torch.cat([x ,mfcc, chroma], 1)
        #print(out.shape)  
        out = self.fc_norm(out)
        out = F.relu(self.fc1(out))
        out = self.dropout(out)
        out = self.fc2(out)
        return x


def _pre_train_resnet(pre_trained=True): 
    from torchvision import models
    if pre_trained:
        model = models.resnet18(pretrained=True)
    else: model = models.resnet18(pretrained=False)

    feature_extractor= nn.Sequential(*list(model.children())[:-2], nn.AdaptiveAvgPool2d((1,1)))
    feature_extractor.eval()
    return feature_extractor