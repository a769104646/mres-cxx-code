import torch
import torch.nn.functional as F
from torch import nn
from torch import optim
from torch.utils.data import TensorDataset, dataloader
import torchbearer
from torchbearer import Trial#
from torch.utils.tensorboard import SummaryWriter
from matplotlib.ticker import MaxNLocator
from collections import OrderedDict
import numpy as np
import matplotlib.pyplot as plt
import os
import glob

from sklearn import metrics

"My designed training framework using torchberer."

class trainer(nn.Module):
    def __init__(self, model, loss_func, device, metrics, trial_name, verbose, opt=optim.Adam, opt_params={},flag=1, **kwargs):  
        super().__init__()
        self.model = model
        self.loss_func = loss_func
        if flag ==1:self.opt = opt(list(self.model.parameters()), **opt_params)
        else:self.opt = opt
        self.metrics = metrics
        self.device = device
        self.trial_path = os.path.join('../logs/', trial_name) 
        self.call_backs = None
        self.verbose = verbose

    def init_trial(self): 

        #initial call_back functions for the trail.
        if self.call_backs:
            self.trial = Trial(self.model,self.opt,self.loss_func,metrics=self.metrics,callbacks=self.call_backs,verbose=self.verbose).to(self.device)
        else:
            self.trial = Trial(self.model,self.opt,self.loss_func,metrics=self.metrics,verbose=self.verbose).to(self.device)
            
    def run(self, epoch, tr_loader,val_loader,t_loader=None, val_steps=None):
        self.trial.with_generators(tr_loader, test_generator=t_loader,val_generator=val_loader,val_steps=val_steps)
        return self.trial.run(epoch)
    

    def save_model(self, model_name, path=None, mkdir=True, **kwargs):
        if mkdir:
            os.makedirs(os.path.dirname(path), exist_ok=True)
        path = os.path.join(path, model_name)
        if path is None:
            path = os.path.join(self.trial_path, model_name)
             
        torch.save(OrderedDict([
            ('model', self.model.state_dict(**kwargs)),
            #('trial', self.trial.state_dict()),
            #('opt',   self.opt.state_dict()),     
            ]), path)
        return path
    
    def load_model(self, f_path, **kwargs):
        if not os.path.exists(f_path):
            print('invalid path')
            return None
        checkpoint = torch.load(f_path)
        self.model.load_state_dict(checkpoint['model'], **kwargs)


    def fit_cuda_TensorDataset(self, m:nn.Module, t_data:TensorDataset, batches=10):
        def chunks(len_d, n):
            l = list(range(len(len_d)))
            for i in range(0,len(l),n): yield l[i:i+n]
        with torch.no_grad():
            y_pre, y_target, y = [],[],[]
            for b in chunks(t_data, batches):
                inputs = torch.stack([t_data[idx][0] for idx in b])
                out = m(inputs.cuda())
                y_pre.extend(out.softmax(1).argmax(1).cpu().numpy())
                y.extend(out.cpu().numpy())
                y_target.extend([t_data[idx][1] for idx in b])
        return y_pre, y_target, y
    
    def plot_fig(self, loss, title, xlabel,ylabel, x_all=False):    
        plt.figure()
        ax = plt.axes()
        ax.set_title(title)
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)
        x = range(len(loss))
        plt.plot(x,loss)
        if x_all:
            ax.xaxis.set_major_locator(MaxNLocator(integer=True))
            plt.xticks(x)
        plt.legend()
        plt.show()

    def plot_figs(self,out,epoch,title,y_label, para=['loss','val_loss']):
        tem =  [ [0]*epoch for i in range(len(para))]
        for idx, label in enumerate(para):
            for i in range(epoch):
                tem[idx][i] = out[i][label]

        plt.figure()
        ax = plt.axes()
        ax.set_title(title)
        ax.set_ylabel(y_label)
        ax.set_xlabel("Epoch")
        for i, item in enumerate(tem):
            plt.plot(range(epoch),item, label=para[i])
        plt.legend()
        plt.show()
