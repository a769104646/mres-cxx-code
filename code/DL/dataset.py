import os
import sys
from librosa.filters import chroma
import torch
import random
import torch.nn.functional as F
from torch.utils.data import Dataset
import pandas as pd
import numpy as np
import torchaudio
import sox
import librosa
from torchaudio import sox_effects
import torchaudio.transforms as T
from einops import rearrange, reduce, repeat

'load dataset'

class MyDataset(Dataset):
    def __init__(self,
                 annot_file,
                 target_sr,
                 num_samples,
                 effects = None,
                 transform:str = None,
                 args:dict = None,
                 if_time_shift:bool = True,
                 if_spectro_augmentation:bool = True, 
                 split_n_chunk:int = None,          
                 if_patches:int = False
                 ):
        
        self.annot = pd.read_csv(annot_file)
        self.effects = effects
        self.transform = transform
        self.target_sr = target_sr
        self.num_samples = num_samples
        self.args = args

        self.split_n_chunk = split_n_chunk
        self.time_shift_rate = 0.2

        self.if_spec_augmentation = if_spectro_augmentation
        self.if_time_shift = if_time_shift
        self.if_patches = if_patches

    def __len__(self):
        return len(self.annot)

    def __getitem__(self, index):
        sample_path = self.get_sample_path(index)
        label = self.get_sample_label(index)
        signal, sr = torchaudio.load(sample_path)

        signal = self.resample(signal, sr)
        signal = self.remix_channels(signal)
        signal = self.cut_if(signal)
        signal = self.padding_if(signal)

        if self.effects:
            if sys.platform == 'win32':
                tem = self.effects.build_array(input_array=signal[0].numpy().copy(),sample_rate_in=self.target_sr)
                signal = torch.tensor(tem).view(1,-1)
            else: 
                signal,_ = sox_effects.apply_effects_tensor(signal,self.target_sr,effects=self.effects)
    
        if self.if_time_shift:
            signal = self.time_shift(signal, self.time_shift_rate)

        if self.transform == 'mel':
            signal = self.mel(signal)

        if self.transform == 'mel_scale':
            signal = self.mel(signal)

        if self.transform ==  'kaldi_fbank':
            signal = self.kaldi_fbank(signal)

        elif self.transform == 'mfcc':
            signal = self.mfcc(signal)
           
        elif self.transform == 'spec':   
            signal = self.spec(signal)  

        elif self.transform == 'mel_mfcc':
            signal = torch.cat([self.mel(signal),self.mfcc(signal)], 0)

        elif self.transform == 'fusion_mel':
            mel  = self.mel(signal)
            mfcc = self.mfcc(signal).squeeze().transpose(0,1).mean(0)
            chroma = self.get_chroma(signal.squeeze())
            return [mel, mfcc, chroma], label

        elif self.transform == 'fusion_mfcc':
            mfcc = self.mfcc(signal)
            mel = self.mel(signal).squeeze().transpose(0,1).mean(0)
            chroma = self.get_chroma(signal.squeeze())
            return [mfcc, mel, chroma], label
        
        if self.if_patches:
            m = signal.size(1) // self.if_patches
            n = signal.size(2) // self.if_patches
            signal = signal[:,:m*self.if_patches,:n*self.if_patches]

        return signal, label



    def split_n_chunk_frame(self, signal, m):
        #shape 1,n_mel, sample 1, 64, 64 *20 = 1280
        w = signal.size(2)//m
        h = signal.size(1)//m
        signal = signal[:,:m*h,:m*w].squeeze()
        #signal = torch.stack(torch.chunk(signal, n, 1))
        signal = rearrange(signal,'(n v) (s k) -> (k v) n s ', k=w,v=h)
        return signal
    
    def spec(self, signal):
        spectrogram = T.Spectrogram(
            n_fft=self.args['n_fft'],
            win_length = self.args['win_length'],
            hop_length = self.args['hop_length'],
            center = True,
            pad_mode = "reflect",
            power=2.0,
        )
        signal = spectrogram(signal)
        
        if self.split_n_chunk:
            signal = self.split_n_chunk_frame(signal, self.split_n_chunk)
        
        return signal

    'log-mel spectrogram'
    def mel(self,signal):
        mel_s = T.MelSpectrogram(
                    sample_rate=self.target_sr,
                    n_fft=self.args['n_fft'],
                    win_length=self.args['win_length'],
                    hop_length=self.args['hop_length'],
                    center=True,
                    pad_mode="reflect",
                    power= 2.0,
                    norm='slaney',
                    onesided=True,
                    n_mels=self.args['n_mels'],
                    mel_scale="htk")
        signal = mel_s(signal)
        #signal = T.AmplitudeToDB(top_db=self.args['top_db'])(signal)
        if self.if_spec_augmentation:
            signal = self.spectro_augment(signal)
        if self.split_n_chunk:
                signal = self.split_n_chunk_frame(signal, self.split_n_chunk)
        return signal
    
    def mel_scale(self, signal):
        mel_scale = T.MelScale(
                    sample_rate=self.target_sr,
                    n_fft=self.args['n_fft'],
                    mel_scale="htk",
                    n_mels=self.args['n_mels'],
                    norm='slaney',
            )
        signal = mel_scale(signal)
        if self.if_spec_augmentation:
            signal = self.spectro_augment(signal)
        if self.split_n_chunk:
                signal = self.split_n_chunk_frame(signal, self.split_n_chunk)
        return signal

    'log-mel spectrogram'
    def kaldi_fbank(self,signal):
        frame_length = self.args['n_fft'] / self.target_sr * 1000.0
        frame_shift = frame_length / 2.0
        params = {
            "channel": 0,
            "dither": 0.0,
            "htk_compat" :True,
            "num_mel_bins": self.args['n_mels'],
            "window_type": "hanning",
            "frame_length": frame_length,
            "frame_shift": frame_shift,
            "use_energy":False,
            "sample_frequency": self.target_sr,
        }
        fbank = torchaudio.compliance.kaldi.fbank(signal, **params)
        return fbank.t().unsqueeze(0)

    def mfcc(self,signal):
        mfcc_t = T.MFCC(
                    sample_rate=self.target_sr,
                    n_mfcc=self.args['n_mfcc'],
                    log_mels = False,
                    melkwargs={
                    'n_fft': self.args['n_fft'],
                    'n_mels': self.args['n_mfcc'],
                    'hop_length': self.args['hop_length'],
                    'mel_scale': 'htk',})
        
        signal = mfcc_t(signal)

        if self.if_spec_augmentation:
            signal = self.spectro_augment(signal)
        if self.split_n_chunk:
                signal = self.split_n_chunk_frame(signal, self.split_n_chunk)
        return signal

    def cut_if(self, signal):
        if signal.shape[1] > self.num_samples:
            signal = signal[:, :self.num_samples]
        return signal

    def padding_if(self, signal):
        signal_len = signal.shape[1]
        if signal_len < self.num_samples:
            padding_dim = (0, self.num_samples - signal_len)
            signal = torch.nn.functional.pad(signal, padding_dim)
        return signal

    def resample(self, signal, sr):
        if sr != self.target_sr:
            resampler = T.Resample(sr, self.target_sr)
            signal = resampler(signal)
        return signal

    def remix_channels(self, signal):
        if signal.shape[0] > 1:
            signal = torch.mean(signal, dim=0, keepdim=True)
        return signal

    def time_shift(self, sig, shift_limit):
        _, sig_len = sig.shape
        shift_amt = int(random.random() * shift_limit * sig_len)
        sig = sig.roll(shift_amt)
        return sig

    def get_chroma(self, signal):
        stft     = np.abs(librosa.stft(signal.numpy()))
        chroma   = np.mean(librosa.feature.chroma_stft(S = stft, sr = self.target_sr).T, axis=0)
        return torch.from_numpy(chroma)

    def spectro_augment(self, spec, max_mask_pct=0.1, n_freq_masks=1, n_time_masks=1):
        _, n_mels, n_steps = spec.shape
        mask_value = spec.mean()
        aug_spec = spec

        freq_mask_param = max_mask_pct * n_mels
        for _ in range(n_freq_masks):
            aug_spec = T.FrequencyMasking(freq_mask_param)(aug_spec, mask_value)

        time_mask_param = max_mask_pct * n_steps
        for _ in range(n_time_masks):
            aug_spec = T.TimeMasking(time_mask_param)(aug_spec, mask_value)
        return aug_spec

    def get_duration(self, index):
        return self.annot.iloc[index].num_frames/self.annot.iloc[index].sr

    def get_sample_path(self, index):
        return self.annot.iloc[index].path

    def get_sample_label(self, index):
        return self.annot.iloc[index].label
    
