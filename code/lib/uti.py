import os
import shutil
from collections import defaultdict
import matplotlib
import matplotlib.pyplot as plt
import time
import librosa
import numpy as np
from IPython.display import Audio, display
import re

def get_f_name(path):
    p_n_list = os.listdir(path)
    if '.DS_Store' in p_n_list: p_n_list.remove('.DS_Store')
    regex = re.compile(r'._.')
    p_n_list = [i for i in p_n_list if not regex.match(i)]
    p_n_list.sort()
    return p_n_list

   
def list_duplicates(seq):
    tally = defaultdict(list)
    for i,item in enumerate(seq):
        tally[item].append(i)
    return ((key,locs) for key,locs in tally.items() 
                            if len(locs)>=1)

def copy_dir_with_f(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)

def play_audio(waveform, sample_rate):
  num_channels, num_frames = waveform.shape
  if num_channels == 1:
    display(Audio(waveform[0], rate=sample_rate))
  elif num_channels == 2:
    display(Audio((waveform[0], waveform[1]), rate=sample_rate))
  else:
    raise ValueError("Waveform with more than 2 channels are not supported.")

def plot_waveform(waveform, sample_rate, title="Waveform",fig_size=(20, 10), xlim=None, ylim=None):
  num_channels, num_frames = waveform.shape
  time_axis = np.arange(0, num_frames) / sample_rate

  figure, axes = plt.subplots(num_channels, 1,figsize=fig_size)
  if num_channels == 1:
    axes = [axes]
  for c in range(num_channels):
    axes[c].plot(time_axis, waveform[c], linewidth=1)
    axes[c].grid(True)
    if num_channels > 1:
      axes[c].set_ylabel(f'Channel {c+1}')
    if xlim:
      axes[c].set_xlim(xlim)
    if ylim:
      axes[c].set_ylim(ylim)
  figure.suptitle(title)
  plt.show(block=False)

def plot_specgram(waveform, sample_rate, title="Spectrogram", fig_size=(20, 10), xlim=None):
    num_channels, num_frames = waveform.shape
    time_axis = np.arange(0, num_frames) / sample_rate

    figure, axes = plt.subplots(num_channels, 1, figsize=fig_size)
    if num_channels == 1:
      axes = [axes]
    for c in range(num_channels):
      axes[c].specgram(waveform[c], Fs=sample_rate)
      if num_channels > 1:
        axes[c].set_ylabel(f'Channel {c+1}')
      if xlim:
        axes[c].set_xlim(xlim)
    figure.suptitle(title)
    plt.show(block=False)


def plot_spectrogram(spec, title=None, ylabel='freq_bin', aspect='auto',fig_size=(20, 10), xmax=None):
    fig, axs = plt.subplots(1, 1, figsize=fig_size)
    axs.set_title(title or 'Spectrogram (db)')
    axs.set_ylabel(ylabel)
    axs.set_xlabel('frame')
    im = axs.imshow(librosa.power_to_db(spec), origin='lower', aspect=aspect)
    if xmax:
      axs.set_xlim((0, xmax))
    fig.colorbar(im, ax=axs)
    plt.show(block=False)